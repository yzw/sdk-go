build:
	go build ./...

lint:
	golangci-lint run ./...

mockgen:
	mockgen -destination ./mock/sdk_mock.go -package mock -source sdk_interface.go

gomod:
	go get chainmaker.org/chainmaker/pb-go/v2@v2.3.3_qc
	go get chainmaker.org/chainmaker/common/v2@v2.3.3_qc
.PHONY: build lint
